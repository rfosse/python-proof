import networkx as nx
import time
import drawit as dw
import header as hd
import threading

# Initialization of the execution time
tps1 = time.clock()

#----------------------------------- GLOBAL VARIABLE ---------------------------------------------------------
nxd = nx.DiGraph()

edgefirstlist = []
edgesecondlist = []
corefirstlist = []
coresecondlist = []

listPath = []
listPathCore = []
listCore = []
lenList = []

FILE = "test/core_bon.tlp"
PARALLEL = False
NB_THREAD = 4


#------------------------------------- END GLOBAL VARIABLE ---------------------------------------------------

#------------------------------------- READING GRAPH ---------------------------------------------------------

for line in open(FILE, "r"):
    data = line.split()
    for g in [nxd]:
        if data[0] == "e":
            edgefirstlist.append(int(data[1]))
            edgesecondlist.append(int(data[2]))
            corefirstlist.append(int(data[3]))
            coresecondlist.append(int(data[4]))


#------------------------------------ END READING GRAPH -------------------------------------------------------

#------------------------------------ MAIN FUNCTIONS ----------------------------------------------------------

def auxSearchTree(index, listUsed, listCore):
    listCore.append(coresecondlist[index])
    hd.appendList(listUsed, edgesecondlist[index])
    value = edgefirstlist[index]
    hd.appendList(listUsed, value)

    if value in edgesecondlist:
        return auxSearchTree(edgesecondlist.index(value), listUsed, listCore)
    else:
        listCore.append(corefirstlist[index])
        lenList.append(len(listUsed))
        listPathCore.append(listCore)
        return listUsed


def searchTree(begin,end):
    list_res = []
    for i in range(begin,end):
        if coresecondlist[i] == -1:
            list_res.append(auxSearchTree(i, [], []))
    return list_res


#------------------------------------ END MAIN FUNCTIONS --------------------------------------------------------



print("Start Create Path")
if(PARALLEL):
    class myThread(threading.Thread):
        def __init__(self, name, begin, end):
            threading.Thread.__init__(self)
            self.name = name
            self.begin = begin
            self.end = end

        def run(self):
            print("Creation du Thread", self.name)
            list_by_thread = searchTree(self.begin, self.end)
            listPath.append(list_by_thread)
            print("Destruction du Thread", self.name)


    threads = []


    def createThread(nbThread, liste):
        size = len(liste)
        for i in range(1, nbThread + 1):
            thread = myThread(i, int(size / nbThread) * (i - 1), int(size / nbThread) * i)
            threads.append(thread)


    # Create new Threads

    createThread(NB_THREAD, edgefirstlist)

    # --------------------END THREAD CREATION------------------------------------------

    # Start new Threads
    for t in threads:
        t.start()

    # Wait for all threads to complete
    for t in threads:
        t.join()

    print("Exiting Main Thread")
else:
    res = searchTree(0,len(edgefirstlist))

    print("Finish Create Path")

    print("Number of Path :", len(res))


tps2 = time.clock()

print("Executing time: ", tps2 - tps1)


#----------------------------------- CREATION HISTOGRAM -------------------------------------------------------------

dw.drawithist(lenList,"Length of the path","Number of path")
dw.drawithist(hd.nbrCores(listPathCore),"Number of unique core","Number of path")
