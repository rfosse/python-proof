import os

import matplotlib.pyplot as plt
import networkx as nx


def read_file_graph(FILE, i):
    checker = False
    net = nx.Graph()
    for line in open(FILE, "r"):
        if "c |  Number of variables:" in line:
            nb_variable = line.split()[5]
        if "c |  Number of clauses:" in line:
            nb_clause = line.split()[5]
        if "c All useful bySize" in line:
            all_useful = line.split()[4]
            checker = True
        if "c All useless bySize" in line:
            all_useless = line.split()[4]
        if "c ProofOnly useful" in line:
            proofonly_useful = line.split()[4]
        if "c ProofOnly useless" in line:
            proofonly_useless = line.split()[4]
        if "c Parallel version" in line:
            nb_solvers = line.split()[3]
        if "c additional topclausessize all" in line:
            topsize_1Op = line.split()[5]
        if "c additional predictionInitialLBD 40" in line:
            predictILBD_40 = line.split()[14]
        if "c additional predictionInitialLBD 10" in line:
            predictILBD_10 = line.split()[14]
        if "c Literals ProofOnly useful" in line:
            lit_proofonly_useful = line.split()[4]
        if "c Literals ProofOnly useless" in line:
            lit_proofonly_useless = line.split()[4]
        if "c additional propagationGood" in line:
            propagation_good = line.split()[4]
        if "c additional propagationBad" in line:
            propagation_bad = line.split()[4]
        if "c additional lifetimeGood" in line:
            lifetimegoodmedian = line.split()[8]
        if "c CPU time" in line:
            cpu_time = line.split()[4]
        if "c restarts" in line:
            restarts = line.split()[3]

    for g in [net]:
        if not checker:
            g.add_node(i, nb_variable=-1, nb_clause=-1, nb_solvers=-1, all_useful=-1,
                       all_useless=-1, proofonly_useful=-1, proofonly_useless=-1,
                       topsize_10p=-1, predictILBD_40=-1, predictILBD_10=-1, lit_proofonly_useful=-1,
                       lit_proofonly_useless=-1, propagation_good=-1, propagation_bad=-1, lifetimegoodmedian=-1,
                       cpu_time=-1,restarts=-1)
        else:
            g.add_node(i, nb_variable=nb_variable, nb_clause=nb_clause, nb_solvers=nb_solvers, all_useful=all_useful,
                       all_useless=all_useless, proofonly_useful=proofonly_useful, proofonly_useless=proofonly_useless,
                       topsize_10p=topsize_1Op, predictILBD_40=predictILBD_40, predictILBD_10=predictILBD_10,
                       lit_proofonly_useful=lit_proofonly_useful, lit_proofonly_useless=lit_proofonly_useless,
                       propagation_good=propagation_good, propagation_bad=propagation_bad,
                       lifetimegoodmedian=lifetimegoodmedian, cpu_time=cpu_time,restarts=restarts)
    return net


def get_list_percent_proof_useful(g):
    tmp = []
    for i in range(g.number_of_nodes() - 1):
        tmp.append(int(int(g.node[i]['proofonly_useful']) / (
            int(g.node[i]['proofonly_useful']) + int(g.node[i]['proofonly_useless'])) * 100))
    return tmp


def get_list_percent_proof_useless(g):
    tmp = []
    for i in range(g.number_of_nodes() - 1):
        tmp.append(int(int(g.node[i]['proofonly_useless']) / (
            int(g.node[i]['proofonly_useful']) + int(g.node[i]['proofonly_useless'])) * 100))
    return tmp


def get_list_initial_useful(g):
    tmp = []
    for i in range(g.number_of_nodes() - 1):
        tmp.append(int(g.node[i]['all_useful']) - int(g.node[i]['proofonly_useful']))
    return tmp


def get_list_percent_initial_useful(g):
    tmp = []
    for i in range(g.number_of_nodes() - 1):
        tmp.append(int((int(g.node[i]['all_useful']) - int(g.node[i]['proofonly_useful'])) / (
            int(g.node[i]['all_useful']) + int(g.node[i]['all_useless'])) * 100))
    return tmp


def get_list_attributes(g, attr):
    tmp = []
    for i in range(g.number_of_nodes() - 1):
        tmp.append(g.node[i][attr])

    return tmp


def get_list_percent_lit_useful(g):
    tmp = []
    for i in range(g.number_of_nodes() - 1):
        tmp.append(int(int(g.node[i]['lit_proofonly_useful']) / (
            int(g.node[i]['lit_proofonly_useless']) + int(g.node[i]['lit_proofonly_useful'])) * 100))
    return tmp


def get_list_percent_lit_useless(g):
    tmp = []
    for i in range(g.number_of_nodes() - 1):
        tmp.append(int(int(g.node[i]['lit_proofonly_useless']) / (
            int(g.node[i]['lit_proofonly_useless']) + int(g.node[i]['lit_proofonly_useful'])) * 100))
    return tmp


def get_list_percent_propagation_good(g):
    tmp = []
    for i in range(g.number_of_nodes() - 1):
        tmp.append(int(int(g.node[i]['propagation_good']) / (
            int(g.node[i]['propagation_good']) + int(g.node[i]['propagation_bad'])) * 100))
    return tmp


def get_size_proof(g):
    tmp = []
    for i in range(g.number_of_nodes() - 1):
        tmp.append(int(g.node[i]['all_useful']) + int(g.node[i]['all_useless']))
    return tmp


def get_size_product_proof(g):
    tmp = []
    for i in range(g.number_of_nodes() - 1):
        tmp.append(int(g.node[i]['proofonly_useful']) + int(g.node[i]['proofonly_useless']))
    return tmp


def get_list_solvers(g):
    tmp = []
    for i in get_list_attributes(g, 'nb_solvers'):
        tmp.append(int(i))
    return tmp


def get_list_colors(list_solvers):
    list_colors = []
    for i in list_solvers:
        if i == 1:
            c = '#DF0101'
        elif i == 2:
            c = '#FF4000'
        elif i == 4:
            c = '#FAAC58'
        elif i == 6:
            c = '#F4FA58'
        elif i == 8:
            c = '#C8FE2E'
        elif i == 10:
            c = '#3ADF00'
        elif i == 12:
            c = '#58FAF4'
        elif i == 14:
            c = '#58ACFA'
        elif i == 16:
            c = '#5858FA'
        else:
            c = 'k'

        list_colors.append(c)
    return list_colors


def draw_basic(l, xlabel, l2, ylabel, list_colors):
    plt.scatter(l, l2, c=list_colors)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()


def draw_basic_log(l, xlabel, l2, ylabel, list_colors):
    plt.scatter(l, l2, c=list_colors)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.xscale('log')
    plt.yscale('log')
    plt.show()


def draw_basic_semilog(l, xlabel, l2, ylabel, list_colors):
    plt.scatter(l, l2, c=list_colors)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.yscale('log')
    plt.show()


def draw_boxplot(l1, l2, ylabel, log):
    plt.boxplot(l2, positions=l1)
    if log:
        plt.yscale('log')
    plt.xlabel("Nombre de solveurs")
    plt.ylabel(ylabel)
    plt.show()


def list_boxplot_proof_size(g,list_solvers):
    tmp = []
    for k in list_solvers:
        tmp_solvers = []
        for i in range(g.number_of_nodes() - 1):
            if int(g.node[i]['nb_solvers']) == k:
                tmp_solvers.append(int(g.node[i]['all_useful']) + int(g.node[i]['all_useless']))
        tmp.append(tmp_solvers)
    return tmp


def list_boxplot_proof_useful(g,list_solvers):
    tmp = []
    for k in list_solvers:
        tmp_solvers = []
        for i in range(g.number_of_nodes() - 1):
            if int(g.node[i]['nb_solvers']) == k:
                tmp_solvers.append(int(int(g.node[i]['all_useful']) / (
                int(g.node[i]['all_useful']) + int(g.node[i]['all_useless'])) * 100))
        tmp.append(tmp_solvers)
    return tmp


def list_boxplot_initial_useful(g,list_solvers):
    tmp = []
    for k in list_solvers:
        tmp_solvers = []
        for i in range(g.number_of_nodes() - 1):
            if int(g.node[i]['nb_solvers']) == k:
                tmp_solvers.append(int((int(g.node[i]['all_useful']) - int(g.node[i]['proofonly_useful'])) / (
                int(g.node[i]['nb_clause'])) * 100))
        tmp.append(tmp_solvers)
    return tmp


def list_boxplot_cpu_time(g,list_solvers):
    tmp = []
    for k in list_solvers:
        tmp_solvers = []
        for i in range(g.number_of_nodes() - 1):
            if int(g.node[i]['nb_solvers']) == k:
                tmp_solvers.append(float(g.node[i]['cpu_time']))
        tmp.append(tmp_solvers)
    return tmp


def list_boxplot_mean_size(g,list_solvers):
    tmp = []
    for k in list_solvers:
        tmp_solvers = []
        for i in range(g.number_of_nodes() - 1):
            if int(g.node[i]['nb_solvers']) == k:
                tmp_solvers.append((int(g.node[i]['lit_proofonly_useful']) + int(g.node[i]['lit_proofonly_useless'])) / (
                int(g.node[i]['all_useful']) + int(g.node[i]['all_useless'])))
        tmp.append(tmp_solvers)
    return tmp

def list_boxplot_topsize(g,list_solvers):
    tmp = []
    for k in list_solvers:
        tmp_solvers = []
        for i in range(g.number_of_nodes() - 1):
            if int(g.node[i]['nb_solvers']) == k:
                tmp_solvers.append(float(g.node[i]['topsize_10p']))
        tmp.append(tmp_solvers)
    return tmp


def list_boxplot_restarts(g,list_solvers):
    tmp = []
    for k in list_solvers:
        tmp_solvers = []
        for i in range(g.number_of_nodes() - 1):
            if int(g.node[i]['nb_solvers']) == k:
                tmp_solvers.append(float(g.node[i]['restarts']*k))
        tmp.append(tmp_solvers)
    return tmp



def main():
    list_solvers = [1, 2, 4, 6, 8, 10, 12, 14, 16]
    directory = "outputs/"
    cpt = 0
    cpt28 = 0
    nxd = nx.Graph()
    nxdspec = nx.Graph()
    SPEC = False
    # Read all graphs #
    for file in os.listdir(directory):
        filename = directory + file
        nxd.add_node(cpt, read_file_graph(filename, cpt).node[cpt])
        cpt += 1

    for i in range(nxd.number_of_nodes() - 1):
        if int(nxd.node[i]['nb_solvers']) == 1 or int(nxd.node[i]['nb_solvers']) == 16 or int(
                nxd.node[i]['nb_solvers']) == 4:
            nxdspec.add_node(cpt28, nxd.node[i])
            cpt28 += 1

    if not SPEC:

        draw_boxplot(list_solvers, list_boxplot_proof_size(nxd,list_solvers), "Taille de la preuve", True)

        draw_boxplot(list_solvers, list_boxplot_proof_useful(nxd,list_solvers), "Pourcentage de clauses utiles",
                     False)

        draw_boxplot(list_solvers, list_boxplot_cpu_time(nxd,list_solvers), "Temps CPU (s)",
                     True)

        draw_boxplot(list_solvers, list_boxplot_initial_useful(nxd,list_solvers),
                     "Pourcentage de clauses initiales utiles",
                     False)

        draw_boxplot(list_solvers, list_boxplot_mean_size(nxd,list_solvers),
                     "Taille moyenne des Clauses produites",
                     True)

        draw_boxplot(list_solvers, list_boxplot_topsize(nxd,list_solvers),"Taille des 10% plus grandes clauses ",True)

        draw_boxplot(list_solvers, list_boxplot_restarts(nxd, list_solvers), "Nombre de restarts ",
                     True)


    else:
        draw_basic(get_size_product_proof(nxd), "Taille de la preuve produite", get_list_percent_proof_useless(nxd),
                   "% de clauses produites inutiles ", get_list_colors(get_list_solvers(nxd)))

        draw_basic(get_size_proof(nxd), "Taille totale de la preuve", get_list_percent_initial_useful(nxd),
                   "% de clauses initales utiles", get_list_colors(get_list_solvers(nxd)))

        draw_basic(get_list_solvers(nxd), "Nombre de solveurs", get_list_percent_initial_useful(nxd),
                   "% de clauses initales utiles", get_list_colors(get_list_solvers(nxd)))

        draw_basic(get_list_solvers(nxd), "Nombre de solveurs", get_list_attributes(nxd, "topsize_10p"),
                   "Taille médiane des 10% all", get_list_colors(get_list_solvers(nxd)))

        draw_basic(get_list_attributes(nxd, "nb_clause"), "Nombre de clauses initiales",
                   get_list_attributes(nxd, "predictILBD_40"), "% Classification LBD 40",
                   get_list_colors(get_list_solvers(nxd)))

        draw_basic(get_list_attributes(nxd, "nb_clause"), "Nombre de clauses initiales",
                   get_list_attributes(nxd, "predictILBD_10"), "% Classification LBD 10",
                   get_list_colors(get_list_solvers(nxd)))

        draw_basic(get_list_attributes(nxd, "nb_clause"), "Nombre de clauses initiales",
                   get_list_attributes(nxd, "topsize_10p"), "Taille mediane des 10% all",
                   get_list_colors(get_list_solvers(nxd)))

        draw_basic(get_list_attributes(nxd, "nb_clause"), "Nombre de clauses initiales",
                   get_list_attributes(nxd, "topsize_10p"), "Taille mediane des 10% all",
                   get_list_colors(get_list_solvers(nxd)))

        draw_basic_log(get_list_attributes(nxd, "lit_proofonly_useless"), "Nombre de litteraux inutiles dans la preuve",
                       get_list_attributes(nxd, "lit_proofonly_useful"), "Nombre de litteraux utiles dans la preuve",
                       get_list_colors(get_list_solvers(nxd)))

        draw_basic_semilog(get_size_proof(nxd), "Taille de la preuve", get_list_percent_lit_useful(nxd),
                           "Pourcentages de littéraux produits utiles", get_list_colors(get_list_solvers(nxd)))

        draw_basic(get_list_percent_lit_useful(nxd),
                   "Pourcentages de littéraux produits utiles", get_list_percent_initial_useful(nxd),
                   "% de clauses initales utiles", get_list_colors(get_list_solvers(nxd)))

        draw_basic(get_list_solvers(nxd), "Nombre de solveurs",
                   get_list_percent_lit_useful(nxd),
                   "Pourcentages de littéraux produits utiles", get_list_colors(get_list_solvers(nxd)))

        draw_basic_semilog(get_size_proof(nxd), "Taille de la preuve",
                           get_list_percent_lit_useless(nxd),
                           "Pourcentages de littéraux produits inutiles", get_list_colors(get_list_solvers(nxd)))

        draw_basic_semilog(get_size_product_proof(nxdspec), "Taille de la preuve produite",
                           get_list_percent_lit_useless(nxdspec),
                           "Pourcentages de littéraux produits inutiles", get_list_colors(get_list_solvers(nxdspec)))

        draw_basic(get_list_initial_useful(nxd), "Nombre de clauses initiales utiles",
                   get_list_percent_proof_useful(nxd),
                   "% de clauses produites utiles ", get_list_colors(get_list_solvers(nxd)))

        draw_basic(get_list_attributes(nxd, "proofonly_useful"), "Nombre de clauses produites utiles",
                   get_list_percent_propagation_good(nxd), "% de propagations utiles",
                   get_list_colors(get_list_solvers(nxd)))

        draw_basic_log(get_size_product_proof(nxd), "Taille de la preuve produite",
                       get_list_attributes(nxd, "lifetimegoodmedian"), "Mediane de la durée de vie des clauses utiles",
                       get_list_colors(get_list_solvers(nxd)))

        draw_basic_log(get_list_attributes(nxd, "nb_clause"), "Nombre de clauses initiales",
                       get_list_attributes(nxd, "lifetimegoodmedian"), "Mediane de la durée de vie des clauses utiles",
                       get_list_colors(get_list_solvers(nxd)))

        draw_basic_semilog(get_list_solvers(nxd), "Nombre de solveurs", get_size_proof(nxd),
                           "Taille totale de la preuve", get_list_colors(get_list_solvers(nxd)))


if __name__ == "__main__":
    main()