import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import re
import os
import sys
import subprocess
from subprocess import call


def read_file_graph(FILE):
    nxd = nx.DiGraph()
    for line in open(FILE, "r"):
        data = line.split()
        for g in [nxd]:
            if data[0] == "n":
                g.add_node(int(data[1]), solvers=int(data[2]), useful=int(data[3]), kcore=int(data[4]))
    return nxd


def number_product_clauses(g):
    cpt = 0
    for i in range(g.number_of_nodes() - 1):
        if g.node[i]['solvers'] != -1:
            cpt += 1
    return cpt


def useless_clauses(g):
    cpt = 0
    for i in range(g.number_of_nodes() - 1):
        if g.node[i]['solvers'] != -1 and g.node[i]['useful'] == 0:
            cpt += 1
    return [int(cpt / number_product_clauses(g) * 100), cpt]


def call_glucose(file_name, valeur, verbose):
    fo = open(os.devnull, "w")
    cmd = ["../glucose", file_name, "-nbsolvers=" + str(valeur)]
    if (verbose == False):
        call(cmd, stdout=fo.fileno())
    else:
        call(cmd)
    g = read_file_graph("core.tlp")
    return g


def number_initial_clauses(g):
    cpt = 0
    cpt_useful = 0
    cpt_useless = 0
    for i in range(g.number_of_nodes() - 1):
        if g.node[i]['solvers'] == -1:
            cpt += 1
            if g.node[i]['useful'] == 1:
                cpt_useful += 1
            else:
                cpt_useless += 1
    return [int(cpt_useless / cpt * 100), cpt, cpt_useful, cpt_useless]


def main_useless(g):
    tuple = useless_clauses(g)
    nb_product = number_product_clauses(g)
    return [tuple[0], tuple[1], nb_product - tuple[1], nb_product]



def percent_useful_winner(g,nbsolvers):
    solver_winner = g.node[g.number_of_nodes() - 1]['solvers']
    data_solvers = []
    for j in range(0,nbsolvers):
        if j != solver_winner:
            cpt = 0
            for i in range(g.number_of_nodes() - 1):
                if g.node[i]['solvers'] == j and g.node[i]['useful'] == 1:
                    cpt += 1
            data_solvers.append(cpt)
    if not data_solvers:
        data_solvers.append(0)
    mean_solvers = int(np.mean(data_solvers))
    cpt_winner = 0
    for i in range(g.number_of_nodes() - 1):
        if g.node[i]['solvers'] == solver_winner and g.node[i]['useful'] == 1:
            cpt_winner += 1

    return cpt_winner - mean_solvers



def median_age(g):
    median_list_useful = []
    median_list_useless = []
    for i in range(g.number_of_nodes() - 1):
        if g.node[i]['solvers'] != -1 and g.node[i]['useful'] == 1:
            median_list_useful.append(i)
        if g.node[i]['solvers'] != -1 and g.node[i]['useful'] == 0:
            median_list_useless.append(i)

    return [np.median(median_list_useful),np.median(median_list_useless)]




def draw_percent(liste, list, ylabel):
    for i in list:
        plt.plot(liste, i)

    plt.ylabel(ylabel)
    plt.xlabel("Number of solvers")
    plt.show()

def draw_percent_log(liste, list, ylabel):
    for i in list:
        plt.plot(liste, i)

    plt.ylabel(ylabel)
    plt.yscale('log')
    plt.xlabel("Number of solvers")
    plt.show()


def main(liste, directory):
    n = 4
    data_useless_global = [[] for i in range(n)]
    data_initial_global = [[] for i in range(n)]
    data_median_global = [[] for i in range(2)]
    data_winner_global = []
    for file in os.listdir(directory):
        print(file)
        data_int_useless = [[] for i in range(n)]
        data_int_initial = [[] for i in range(n)]
        data_int_median = [[] for i in range(2)]
        data_int_winner = []
        if file.endswith(".gz"):
            file_name = directory + file
            for valeur in liste:
                if VERBOSE:
                    g = call_glucose(file_name, valeur, True)
                else:
                    g = call_glucose(file_name, valeur, False)

                data_initial = number_initial_clauses(g)
                data_useless = main_useless(g)
                data_median = median_age(g)
                data_int_winner.append(percent_useful_winner(g,valeur))
                g.clear()

                data_int_median[0].append(data_median[0])
                data_int_median[1].append(data_median[1])
                for i in range(n):
                    data_int_useless[i].append(data_useless[i])
                    data_int_initial[i].append(data_initial[i])

        for i in range(n):
            data_useless_global[i].append(data_int_useless[i])
            data_initial_global[i].append(data_int_initial[i])

        data_median_global[0].append(data_int_median[0])
        data_median_global[1].append(data_int_median[1])

        data_int_winner.pop(0)
        data_winner_global.append(data_int_winner)



    print("Pourcentage de clauses produites inutiles :", data_useless_global[0])
    print("Nombre de clauses produites :", data_useless_global[3])
    print("Nombre de clauses produites utiles :", data_useless_global[2])
    print("Nombre de clauses produites inutiles :", data_useless_global[1])
    print("-------------------------------------------------------------------")
    print("Pourcentage de clauses initiales inutiles :", data_initial_global[0])
    print("Nombre de clauses initiales :", data_initial_global[1])
    print("Nombre de clauses initiales utiles :", data_initial_global[2])
    print("Nombre de clauses initiales inutiles :", data_initial_global[3])
    print("-------------------------------------------------------------------")
    print("Mediane des ages des clauses produites utiles :", data_median_global[0])
    print("Mediane des ages des clauses produites inutiles :", data_median_global[1])
    print("-------------------------------------------------------------------")
    print("Nb clauses utiles dans le solveur gagnant w.r.t moyenne des solveurs", data_winner_global)


    draw_percent_log(liste, data_initial_global[1], "Number of initial clauses")
    draw_percent_log(liste, data_initial_global[3], "Number of useless initial clauses")
    draw_percent_log(liste, data_useless_global[1], "Number of useless product clauses")

    draw_percent(liste, data_useless_global[0], "% of useful initial clauses")
    draw_percent(liste, data_initial_global[0], "% of useless initial clauses")
    list_winner = [2,3,4,5]
    draw_percent_log(liste, data_median_global[0], "Median of ages of useful product clauses")
    draw_percent_log(liste, data_median_global[1], "Median of ages of useless product clauses")
    draw_percent_log(list_winner, data_winner_global, "Nb useful clauses w.r.t mean solvers")


LIST = [4]
DIRECTORY = "benchs_test/"
VERBOSE = False

if __name__ == "__main__":
    main(LIST, DIRECTORY)
