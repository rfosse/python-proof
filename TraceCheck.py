import matplotlib.pyplot as plt
import networkx as nx
import sys
import numpy as np
import statistics as sts

nxd = nx.DiGraph()
g =nx.DiGraph()
h = nx.DiGraph()

def read_file_star(graph,filename):
    if filename is None:
        return None
    for line in open(filename, "r"):
        data = line.split()
        if data[1] == "*":
            graph.add_node(int(data[0]), init=0)
            j=2
            while(int(data[j])!=0):
                if(int(data[0]) != int(data[j])):
                    graph.add_edge(int(data[0]),int(data[j]))
                j+=1
        else:
            graph.add_node(int(data[0]), init=1)


def read_file(graph,filename):
    if filename is None:
        return None
    for line in open(filename, "r"):
        data = line.split()
        i=1
        cpt=0
        init = find_zero(data)
        if init is False:
            graph.add_node(int(data[0]), init=1)
        else:
            graph.add_node(int(data[0]), init=0)
            while (int(data[init]) != 0):
                if (int(data[0]) != int(data[init])):
                    graph.add_edge(int(data[0]), int(data[init]))
                init += 1



def find_zero(data):
    if data.count("0") != 2:
        return -1
    for i in range(len(data)):
        for j in range(len(data)):
            if int(data[i]) == 0 and int(data[j]) == 0 and i != j and abs(i - j) > 1:
                return min(i,j)+1
    return False


def k_core(g,h):
    if h is None:
        return nx.k_core(g)
    else:
        return 0

def is_isomorphic(g,h):
    if h is None:
        return -1
    return nx.is_isomorphic(g,h)

def average_degree_connectivity(g,h):
    if h is None:
        return nx.average_degree_connectivity(g)
    else:
        av1 = nx.average_degree_connectivity(g)
        av2 = nx.average_degree_connectivity(h)
        return abs(av1-av2)

def is_weakly_connected(g):
    return nx.is_weakly_connected(g)

def diameter(g,h):
    if h is None:
        return nx.diameter(g)
    else:
        d1 = nx.diameter(g)
        d2 = nx.diameter(h)
        return abs(d1-d2)

def nb_initial(g):
    cpt=0
    i=1
    while(i<g.number_of_nodes()):
        if g.node[i]['init'] == 1:
            cpt+=1
        i+=1
    return cpt

def edges_initial(g):
    i=1
    list_res = []
    while i<g.number_of_nodes()-1:
        if g.node[i]['init'] == 1:
            list_res.append(g.in_degree(i))
        i+=1
    return list_res



def tab_solo(g):
    h = g.to_undirected()
    print("------------------------------------------------------------------------------")
    print("----------------------Etude de la preuve--------------------------------------")
    #print("Nombre de clauses initiales : ", nb_initial(g))
    print("Nombre de dépendances : ", g.number_of_edges())
    #print("Nombre de dépendances par clauses initiales :",edges_initial(g))

    print("------------------------------------------------------------------------------")
    print("----------------------Etude basique du Graphe---------------------------------")
    print("Nombre de sommets :", g.number_of_nodes())
    print("Nombre d'arêtes :", g.number_of_edges())
    print("Degrée median :",sts.median(nx.degree(g).values()))
    if(nx.is_connected(h)):
        print("le graphe est connexe")
        #print("Diamètre du graphe : ", diameter(h,None))
        #print("Rayon du graphe : ", nx.radius(h))
    else:
        print("Le graphe n'est pas connexe")
    print("------------------------------------------------------------------------------")
    print("----------------------Etude du DAG--------------------------------------------")

    print("Le graphe de dépendance est un DAG :", is_directed_acyclic_graph(g))
    if(is_directed_acyclic_graph(g)):
        print("Le plus long chemin dans le DAG est de taille :", nx.dag_longest_path_length(g))

    print("------------------------------------------------------------------------------")
    print("----------------------Calcul de mesures---------------------------------------")
    print("Coefficient moyen du clustering de g: ", nx.average_clustering(h))
    print("Pagerank median des sommets de g: ", sts.median(nx.pagerank(g).values()))
    print("K_core median du graphe g: ",sts.median(nx.core_number(g).values()))
    print("------------------------------------------------------------------------------")

def tab_compare(g,h):
    tab_solo(g)
    tab_solo(h)
    print("----------------------Comparaison des traces:---------------------------------")
    print("Isomorphique ?", is_isomorphic(g,h))
    if(is_isomorphic(g,h) is False):
        print("Graphe induit maximal contenant les sommets de degré k ou plus isomorphe: ", is_isomorphic(k_core(g,None),k_core(h,None)))

    return 0

def tab_print(g,h):
    if h.number_of_nodes() == 0:
        tab_solo(g)
    else:
        tab_compare(g,h)
    return 0

def is_directed_acyclic_graph(g):
    return nx.is_directed_acyclic_graph(g)


def main():
    if len(sys.argv) == 1:
        return -1
    elif len(sys.argv) == 2:
        tracecheck1 = str(sys.argv[1])
        tracecheck2 = None
    else:
        tracecheck1 = str(sys.argv[1])
        tracecheck2 = str(sys.argv[2])

    read_file_star(g,tracecheck1)
    read_file(h,tracecheck2)
    tab_print(g,h)
    return 0




if __name__ == "__main__":
    main()
