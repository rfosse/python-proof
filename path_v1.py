import networkx as nx
import time
import sys
import numpy as np

# Initialization of the execution time
tps1 = time.clock()

FILE_NAME = str(sys.argv[1])

nxd = nx.DiGraph()
nodelist = []
edgelist = []
compteur = 0
compteurcore = 0
NB_SOLVERS = 10

STAT = False
GRAPH_FILE = True
# ------------------------------------- READING GRAPH ---------------------------------------------------------

# print("Begin Reading Graph")
if (GRAPH_FILE):
    for line in open(FILE_NAME, "r"):
        data = line.split()
        for g in [nxd]:
            if data[0] == "n":
                g.add_node(int(data[1]), origin=int(data[2]), useful=int(data[3]), pr=float(data[4]), core=int(data[5]),
                           kcore=int(data[6]))
            elif data[0] == "e":
                g.add_edge(int(data[1]), int(data[2]))
            else:
                print("erreur dans l'encodage")
else:
    g = nx.gn_graph(10)


# print("End Reading Graph")
# ------------------------------------ END READING GRAPH -------------------------------------------------------

# ------------------------------------ MAIN FUNCTIONS ----------------------------------------------------------


def originuseful(g):
    cpt = 0
    cpttotal = 0
    for i in range(g.number_of_nodes() - 1):
        if g.node[i]['origin'] == 1:
            cpttotal += 1
        if g.node[i]['useful'] == 1 and g.node[i]['origin'] == 1:
            cpt += 1
    return int(cpt / cpttotal * 100)


def searchTreeCore(g, size, target,nbsolvers):
    arr = [0] * size
    arr_core = [0] * (nbsolvers+1)
    arr[target] = 1
    arr_core[g.node[target]['core']+1] += 1
    for i in range(target - 1, 0, -1):
        arr[i] = 0
        arr_core[g.node[i]['core']+1] += 1
        for j in g.neighbors(i):
            arr[i] += arr[j]
    return [arr, arr_core]



def searchTree(g, size, target):
    arr = [0] * size
    arr[target] = 1
    for i in range(target - 1, 0, -1):
        arr[i] = 0
        for j in g.neighbors(i):
            arr[i] += arr[j]
    return arr



def searchTreeArg(g, size, target,nbsolvers,arg):
    arr = [0] * size
    arr_arg = [0] * (nbsolvers+1)
    list_arg = [0] * (nbsolvers+1)
    arr[target] = 1
    coreres = g.node[target]['core']
    list_arg[coreres] += g.node[target][arg]
    arr_arg[coreres] += 1
    for i in range(target - 1, 0, -1):
        arr[i] = 0
        if g.node[i]['core'] < nbsolvers:
            list_arg[g.node[i]['core']+1] += g.node[i][arg]
            arr_arg[g.node[i]['core']+1] += 1
        else:
            print(g.node[i])
        for j in g.neighbors(i):
            arr[i] += arr[j]

    mean_kcore = []
    for i in range(0,len(list_arg)):
        if arr_arg[i]!=0:
            mean_kcore.append((list_arg[i]/arr_arg[i]))
        else:
            mean_kcore.append(0)
    return [arr,mean_kcore]





def nbrCore(g,nbsolvers,path):
    arr = [0] * nbsolvers
    for p in path:
        j = g.node[p]['core']
        arr[j] += 1
    return arr


# Topological Order
h = nx.Graph()
for j in nx.topological_sort(g):
    h.add_node(j, origin=g.node[j]['origin'], useful=g.node[j]['useful'], pr=g.node[j]['pr'], core=g.node[j]['core'],
               kcore=g.node[j]['kcore'])

h.add_edges_from(g.edges())

# ------------------------------------ END MAIN FUNCTIONS --------------------------------------------------------


if STAT:
    sys.stdout.write(str(sys.argv[1]))
    sys.stdout.write(' ')
    sys.stdout.write(str(g.number_of_nodes()))
    sys.stdout.write(' ')
    sys.stdout.write(str(g.number_of_edges()))
    sys.stdout.write(' ')

    table_paths = searchTreeArg(h, g.number_of_nodes(), g.number_of_nodes() - 1,NB_SOLVERS,'kcore')
    paths = sum(table_paths[0])
    sys.stdout.write(str(paths))
    sys.stdout.write(' ')
    sys.stdout.write(str(table_paths[1]))
    sys.stdout.write('\n')


    # Percent of useful clause in original clause
    #percent = originuseful(g)
    #print(percent)
else:
    print(g.number_of_nodes())
    print(g.number_of_edges())
    res1 = searchTreeArg(h, g.number_of_nodes(), g.number_of_nodes() - 1,NB_SOLVERS,'kcore')
    print(sum(res1[0]))
    # Percent of useful clause in original clause
    percent = originuseful(g)
    print(percent)
