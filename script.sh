#!/usr/bin/env bash

[[ ${BASH_VERSION:0:1} -lt 4 ]] && echo this script requires bash version 4 && exit 1



for graph_file in test/launch/*.tlp ; do
    test -f "$graph_file" || continue # skip folders
    echo $graph_file
    python path_v1.py $graph_file >> test/launch/stat.txt

done

