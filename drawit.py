import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import re
import os
import sys
nxd = nx.DiGraph()

FILE_NAME = "test/" + str(sys.argv[1]) + "/"
EXT = "stat.txt"
NB_FILE = len([name for name in os.listdir(FILE_NAME)]) -1
GRAPH_NAME = True


list_path = []
list_percent = []

nb_cores = [2,3,4,5,6,7,8,9,10]
colors = ["#ff6666","#ff8c66","#ffb366","#ffd966","#ffff66","#d9ff66","#66ffff","#66d9ff","#6666ff","#b8860b","#00008b","#008b8b","#b8860b","#006400"]
graph_name = []
length_paths = []

FILE = FILE_NAME +EXT
for line in open(FILE, "r"):
    data = line.split()
    #g.add_node(int(cpt),name=int(data[0]),nodes=int(data[1]),edges=float(data[2]),paths=int(data[3]),altcore=int(data[4]),originuseful=int(data[5]),totalorigin=int(data[6]))
    if GRAPH_NAME:
        m = re.search(r"(?=[a-z]+)\w+", data[0])
            #print(m.group(0))
        graph_name.append(m.group(0))
    list_path.append(int(data[3]))
    length_paths.append(int(data[1]))


def drawithist(liste, legendex, legendey):
    ax = plt.subplot()
    num_bins = 40
    ax.hist(liste, num_bins, alpha=0.5, color='red', orientation='vertical')
    ax.set_xlabel(legendex)
    ax.set_ylabel(legendey)
    ax.set_title(r'Histogram')
    plt.grid(True)
    plt.tight_layout()
    plt.show()


def Number_file(nb_file):
    cpt = 0
    list_res = []
    for i in range(nb_file):
        sublist_res = []
        for j in range(0,9):
            sublist_res.append(list_path[cpt])
            cpt += 1
        list_res.append(sublist_res)
    return list_res

def drawitPathCore(paths,colors,nb_file):
    pas = 0
    for i in range(0,nb_file):
        if GRAPH_NAME:
            plt.plot(nb_cores, paths[i], colors[i],label=str(graph_name[pas]),linewidth=0.5)
            plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        else:
            plt.plot(nb_cores, paths[i], colors[i], linewidth=0.5)
        pas += 10
    plt.xlabel("nombre de solvers")
    plt.ylabel("nombre de chemins")
    plt.yscale('log')
    plt.show()

def drawitPercent(list_abs):
    plt.plot(list_abs, list_percent, 'ro')
    plt.xlabel("Numéro de Fichier")
    plt.ylabel("Pourcentage de clauses d'origines utile")
    l = plt.axhline(y=np.mean(list_percent),label="Moyenne")
    plt.legend()
    plt.show()


def drawitLength(liste_abs):
    plt.plot(liste_abs,length_paths)
    plt.xlabel("Numéro de fichier")
    plt.ylabel("Nombre de Noeuds")
    plt.show()


def drawitPaths(liste_abs,liste_ord):
    plt.plot(liste_abs,liste_ord, 'ro')
    l = plt.axhline(y=np.mean(liste_ord), label="Moyenne",color="red")
    plt.xlabel("Numéro de solvers")
    plt.ylabel("Nombre de chemins")
    plt.yscale('log')
    plt.legend()
    plt.show()

#list_res = Number_file(5)
#print(list_res)
#drawitPathCore(list_res,colors,9)
#drawitPercent()



def create_list(size):
    res = []
    for i in range(size):
        res.append(i)
    return res



#drawitPaths(create_list(NB_FILE),list_path)
#drawitPercent(create_list(NB_FILE))
#drawitLength(create_list(NB_FILE))

drawitPaths(create_list(NB_FILE),list_path)