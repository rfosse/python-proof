import threading
# -------------------------------------THREAD CREATION-----------------------------------------------------------

class myThread(threading.Thread):
    def __init__(self, name, begin, end):
        threading.Thread.__init__(self)
        self.name = name
        self.begin = begin
        self.end = end

    def run(self):
        print("Creation du Thread", self.name)
        print("Destruction du Thread", self.name)

threads = []

def createThread(nbThread, liste):
    size = len(liste)
    for i in range(1, nbThread + 1):
        thread = myThread(i, int(size / nbThread) * (i - 1), int(size / nbThread) * i)
        threads.append(thread)


# Create new Threads

createThread(6, [])


# --------------------END THREAD CREATION------------------------------------------

# Start new Threads
for t in threads:
    t.start()


# Wait for all threads to complete
for t in threads:
    t.join()


print("Exiting Main Thread")
