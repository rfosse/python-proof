#---------------------------------- BASIC FUNCTIONS ----------------------------------------------------

def len2(liste):
    somme = 0
    for i in range(len(liste)):
        if liste[i] != 0:
            somme += 1
    return somme

def lenlistdouble(l):
    list = []
    for i in l:
        for j in i:
            list.append(j)
    return len(list)

def moyenne(liste):
    if liste: return float(sum(liste)) / len2(liste)


def max2(liste):
    val_max = 0
    for i in range(len(liste)):
        if val_max < liste[i]:
            val_max = liste[i]
    return val_max

def onelist(l):
    return [item for sublist in l for item in sublist]

def nbrCoreDifferent(l):
    res = []
    for i in l:
        res.append(i.corefirst)
    return len(set(res))

def listCoreDifferent(l):
    listereturn = []
    for i in l:
        listereturn.append(nbrCoreDifferent(i))
    return listereturn

def nbrCores(liste):
    listCore = []
    for i in liste:
        listCore.append(len(set(i)))
    return listCore


def nbrCoreRepet(liste):
    listCore = []
    for i in liste:
        listCore.append(len(i))
    return listCore


#------------------------------------ END BASIC FUNCTIONS --------------------------------------------

#------------------------------------ FUNCTION ON LISTS ----------------------------------------------


def getElementByIndex(liste,val):
    for i in liste:
        if i.id == val:
            return i


def getElementBySecondEdge(liste,val):
    for i in liste:
        if i.edgesecond == val:
            return i
    return False

def existElementBySecondCore(i,val,liste):
    index = liste[i]
    if index.coresecond == val:
            return index
    return False

def getElementBySecondCore(i,val,liste):
    index = liste[i]
    if index.coresecond == val:
            return index

def existElementInSecondEdge(liste,val):
    for i in liste:
        if i.edgesecond == val:
            return True
    return False

def appendList(liste,value):
    if len(liste) > 0:
        last = liste.pop()
        if last != value:
            liste.append(last)
    liste.append(value)


#-------------------------------------------------------------------------------------------


class Node:
    def __init__(self,id,origin,useful,pr,core,kcore):
        self.id = id
        self.origin = origin #original Clause
        self.useful = useful #Clause has been used in the proof
        self.pr = pr #page ranking
        self.core = core
        self.kcore = kcore

class Edge:
    def __init__(self,node1,node2):
        self.node1 = node1
        self.node2 = node2



#--------------------------------------------------------------------------------------------