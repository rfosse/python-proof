import matplotlib.pyplot as plt
import networkx as nx
import numpy as np

solvers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 50]
solver10 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
CONFLICT = False
USELESS = False
TIME = False
PROPAGATION = False
TEST = True


def mean_sublist(list_param):
    list_res = []
    for i in list_param:
        list_res.append(np.mean(i))
    return list_res


def arrays_from_file(filename):
    """Builds a list of variable length arrays from a comma-delimited text file"""
    output = []
    with open(filename, 'r') as infile:
        for line in infile:
            line = np.array(line.strip().split(' '), dtype=np.int)
            output.append(line)
    return output

def arrays_from_file_float(filename):
    """Builds a list of variable length arrays from a comma-delimited text file"""
    output = []
    with open(filename, 'r') as infile:
        for line in infile:
            line = np.array(line.strip().split(' '), dtype=np.float)
            output.append(line)
    return output


def arrayMean(array):
    output = []
    for i in array:
        output.append(np.mean(i, dtype=np.int))
    return output


def read_file_useless(file_arg):
    list_conflict = []
    for line in open(file_arg, "r"):
        data = line.split()
        list_conflict_tmp = []
        for i in range(0, len(data)):
            list_conflict_tmp.append(float(data[i]))
        list_conflict.append(list_conflict_tmp)
    return list_conflict


def drawIt(data, labels,xlabel, ylabel):
    plt.boxplot(data, labels=labels, showfliers=False)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()

def drawItLog(data, labels,xlabel, ylabel):
    plt.boxplot(data, labels=labels, showfliers=False)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.yscale('log')
    plt.show()


def drawMean(data,labels,xlabel,ylabel):
    plt.plot(labels, np.mean(data, axis=1))
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()

def drawMeanfloat(data,labels,xlabel,ylabel):
    plt.plot(labels, np.mean(data, axis=1,dtype=np.float))
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()


def drawMeans(list1, list2, list3, list4, list5, list6, labels):
    plt.plot(labels, list1, label="cmu-bmc-barrel6")
    plt.plot(labels, list2, label="een-pico-prop05-50")
    plt.plot(labels, list3, label="hoons-vbmc-lucky7")
    plt.plot(labels, list4, label="comb1.shuffled")
    plt.plot(labels, list5, label="rand_net60-25-10.shuffled")
    plt.yscale('log')
    plt.legend()
    plt.show()


def subDrawMean(array1, array2, array3, array4, array5, array6, labels):
    plt.subplot(3, 2, 1)
    plt.title('cmu-bmc-barrel6')
    plt.yscale('log')
    plt.plot(labels, arrayMean(array1), label="cmu-bmc-barrel6")

    plt.subplot(3, 2, 2)
    plt.title('een-pico-prop05-50')
    plt.plot(labels, arrayMean(array2), label="een-pico-prop05-50")

    plt.subplot(3, 2, 3)
    plt.title('hoons-vbmc-lucky7')
    plt.yscale('log')
    plt.plot(labels, arrayMean(array3), label="hoons-vbmc-lucky7")

    plt.subplot(3, 2, 4)
    plt.title('comb1.shuffled')
    plt.yscale('log')
    plt.plot(labels, arrayMean(array4), label="comb1.shuffled")

    plt.subplot(3, 2, 5)
    plt.title('dekker.used-as.sat04-989')
    plt.yscale('log')
    plt.plot(labels, arrayMean(array5), label="decker.used-as.sat04-989")

    plt.subplot(3, 2, 6)
    plt.title('rand_net60-25-10.shuffled')
    plt.yscale('log')
    plt.plot(labels, arrayMean(array6), label="rand_net60-25-10.shuffled")

    plt.tight_layout()

    plt.show()


def subdrawMeanUseful(list1,list2,list3,list4,labels):
    plt.subplot(2, 2, 1)
    plt.title('cmu-bmc-barrel6')
    plt.plot(labels, arrayMean(list1), label="cmu-bmc-barrel6")

    plt.subplot(2, 2, 2)
    plt.title('decker.used-as.sat04-989')
    plt.plot(labels, arrayMean(list2), label="decker.used-as.sat04-989")

    plt.subplot(2, 2, 3)
    plt.title('hoons-vbmc-lucky7')
    plt.plot(labels, arrayMean(list3), label="hoons-vbmc-lucky7")

    plt.subplot(2, 2, 4)
    plt.title('rand_net60-25-10.shuffled')
    plt.plot(labels, arrayMean(list4), label="rand_net60-25-10.shuffled")

    plt.tight_layout()
    plt.show()



def subDrawIt(list1, list2, list3, list4, list5, list6, labels):
    plt.subplot(3, 2, 1)
    plt.title('cmu-bmc-barrel6')
    plt.yscale('log')
    plt.boxplot(list1, labels=labels, showfliers=False)

    plt.subplot(3, 2, 2)
    plt.title('een-pico-prop05-50')
    plt.boxplot(list2, labels=labels, showfliers=False)

    plt.subplot(3, 2, 3)
    plt.title('hoons-vbmc-lucky7')
    plt.yscale('log')
    plt.boxplot(list3, labels=labels, showfliers=False)

    plt.subplot(3, 2, 4)
    plt.title('comb1.shuffled')
    plt.yscale('log')
    plt.boxplot(list4, labels=labels, showfliers=False)

    plt.subplot(3, 2, 5)
    plt.title('dekker.used-as.sat04-989')
    plt.yscale('log')
    plt.boxplot(list5, labels=labels, showfliers=False)

    plt.subplot(3, 2, 6)
    plt.title('rand_net60-25-10.shuffled')
    plt.yscale('log')
    plt.boxplot(list6, labels=labels, showfliers=False)

    plt.tight_layout()

    plt.show()


if CONFLICT:
    array_cmu = arrays_from_file('conflicts/res_conflicts/cmu.txt')
    array_een = arrays_from_file('conflicts/res_conflicts/een.txt')
    array_hoons = arrays_from_file('conflicts/res_conflicts/hoons.txt')
    array_comb = arrays_from_file('conflicts/res_conflicts/comb1.txt')
    array_rand = arrays_from_file('conflicts/res_conflicts/rand.txt')
    array_decker = arrays_from_file('conflicts/res_conflicts/decker.txt')

    subDrawIt(array_cmu, array_een, array_hoons, array_comb, array_decker, array_rand, solvers)

    subDrawMean(array_cmu, array_een, array_hoons,
                array_comb, array_decker, array_rand, solvers)

if USELESS:
    array_useless_rand = arrays_from_file_float("conflicts/res_useful/rand.txt")
    array_useless_cmu = arrays_from_file_float("conflicts/res_useful/cmu.txt")
    array_useless_hoons = arrays_from_file_float("conflicts/res_useful/hoons.txt")
    array_useless_decker = arrays_from_file_float("conflicts/res_useful/decker.txt")
    #drawIt(array_useless_rand, solver10,"nombre de solveurs","% de clauses intiales utile")
    drawMean(array_useless_rand,solver10,"nombre de solveurs","% de clauses initiales utile")
    drawMean(array_useless_cmu, solver10, "nombre de solveurs", "% de clauses initiales utile")
    drawMean(array_useless_hoons, solver10, "nombre de solveurs", "% de clauses initiales utile")
    drawMean(array_useless_decker, solver10, "nombre de solveurs", "% de clauses initiales utile")

    subdrawMeanUseful(array_useless_cmu,array_useless_decker,array_useless_hoons,array_useless_rand,solver10)


if TIME:
    array_time_rand = arrays_from_file_float('conflicts/res_time/rand.txt')
    array_time_cmu = arrays_from_file_float('conflicts/res_time/cmu.txt')
    array_time_decker = arrays_from_file_float('conflicts/res_time/decker.txt')
    array_time_hoons = arrays_from_file_float('conflicts/res_time/hoons.txt')
    array_time_comb1 = arrays_from_file_float('conflicts/res_time/comb1.txt')
    #drawMeanfloat(array_time_cmu, solver10, "nombre de solveurs", "temps écoulé (s)")

    drawIt(array_time_cmu,solver10,"nombre de solveurs","temps écoulé (s)")
    drawIt(array_time_rand, solver10, "nombre de solveurs", "temps écoulé (s)")
    drawIt(array_time_decker, solver10, "nombre de solveurs", "temps écoulé (s)")
    drawIt(array_time_hoons, solver10, "nombre de solveurs", "temps écoulé (s)")
    drawIt(array_time_comb1, solver10, "nombre de solveurs", "temps écoulé (s)")

    subDrawIt(array_time_cmu, array_time_cmu, array_time_hoons, array_time_comb1, array_time_decker, array_time_rand, solver10)

if PROPAGATION:
    array_propagate_cmu = arrays_from_file_float('conflicts/res_propagations/cmu.txt')
    array_propagate_decker = arrays_from_file_float('conflicts/res_propagations/decker.txt')
    array_propagate_rand = arrays_from_file_float('conflicts/res_propagations/rand.txt')
    array_propagate_hoons = arrays_from_file_float('conflicts/res_propagations/hoons.txt')
    array_propagate_comb1 = arrays_from_file_float('conflicts/res_propagations/comb1.txt')


    drawItLog(array_propagate_cmu, solver10, "nombre de solveurs", "nombre de propagations")
    drawIt(array_propagate_decker, solver10, "nombre de solveurs", "nombre de propagations")
    drawItLog(array_propagate_rand, solver10, "nombre de solveurs", "nombre de propagations")
    drawItLog(array_propagate_hoons, solver10, "nombre de solveurs", "nombre de propagations")
    drawItLog(array_propagate_comb1, solver10, "nombre de solveurs", "nombre de propagations")

    subDrawIt(array_propagate_cmu, array_propagate_cmu, array_propagate_hoons, array_propagate_comb1, array_propagate_decker, array_propagate_rand,
              solver10)

if TEST:
    array_cmu = arrays_from_file('conflicts/res_propagations/cmu.txt')
    array_new_cmu = arrays_from_file('conflicts/new/cmu_propagation.txt')

    drawIt(array_cmu, solver10, "nombre de solveurs", "nbr conflits")

    drawIt(array_new_cmu, solver10, "nombre de solveurs", "nbr conflits")